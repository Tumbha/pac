# coding: utf-8
import curses
import locale
from curses import wrapper

locale.setlocale(locale.LC_ALL,"")

def main(stdscr): #WAHOUH
    # Clear screen
    stdscr.clear()

    # This raises ZeroDivisionError when i == 10.
    stdscr.refresh()
    #stdscr.getkey()
    curses.init_pair(1, curses.COLOR_RED, curses.COLOR_WHITE)
    x = y = 0
    while True:
        c = stdscr.getch()
        if c == ord('p'):
            break
        elif c == curses.KEY_HOME:
            x = y = 0
        elif c == ord('q'):
            y -= 1
            stdscr.addstr(x,y, "é", curses.color_pair(0))
        elif c == ord('d'):
            y += 1
            stdscr.addstr(x,y, "X", curses.color_pair(0))
        elif c == ord('s'):
            x += 1
            stdscr.addstr(x,y, "X", curses.color_pair(0))
        elif c == ord('z'):
            x -= 1
            stdscr.addstr(x,y, "X", curses.color_pair(0))
        stdscr.refresh()



if __name__ == "__main__":
    wrapper(main)
